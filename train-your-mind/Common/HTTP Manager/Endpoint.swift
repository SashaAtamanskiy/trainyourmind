//
//  Endpoint.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 30.04.17.
//  Copyright © 2017 Sasha Atamanskiy. All rights reserved.
//

import Foundation
import Alamofire

enum Endpoint
{
    static let baseUrl = URL(string: "http://0.0.0.0:8180")
    
    case login
    case signUp
    case getCurrentUser
    case updateUser
    
    var httpMethod: Alamofire.HTTPMethod {
        switch self {
        case .signUp, .updateUser:
            return .post
        default:
            return .get
        }
    }
    
    fileprivate var path: String {
        switch self {
        case .login:
            return "/users/login"
        case .signUp:
            return "/users/register"
        case .getCurrentUser:
            return "/users/current"
        case .updateUser:
            return "/users/update"
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .signUp, .updateUser:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    
    var url: URL {
        let baseUrl = Endpoint.baseUrl
        return baseUrl!.appendingPathComponent(path)
    }

}
