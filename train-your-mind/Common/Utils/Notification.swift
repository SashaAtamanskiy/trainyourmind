//
//  Notification.swift
//  train-your-mind
//
//  Created by sasha ataman on 10.05.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import LNRSimpleNotifications

class Notification {
    
    fileprivate let nm = LNRNotificationManager()
    
    static let shared = Notification()
    
    init() {
        setup()
    }
    
    func setup() {
        nm.notificationsTitleTextColor = .white
        nm.notificationsBodyTextColor = .white
        nm.notificationsTitleFont = UIFont(name: "SFUIText-Regular", size: 17)!
        nm.notificationsBodyFont = UIFont(name: "SFUIText-Regular", size: 14)!
    }
    
    func showErrorAler(with title: String, body: String) {
        nm.notificationsBackgroundColor = UIColor().hexStringToUIColor(hex: "B00000")
        nm.notificationsIcon = #imageLiteral(resourceName: "alert")
        nm.showNotification(notification: .init(title: title, body: body, duration: LNRNotificationDuration.default.rawValue, onTap: nil, onTimeout: nil))
    }
    
    func showSuccessAlert(with title: String, body: String) {
        nm.notificationsBackgroundColor = UIColor().hexStringToUIColor(hex: "006400")
        nm.notificationsIcon = #imageLiteral(resourceName: "success")
        nm.showNotification(notification: .init(title: title, body: body, duration: LNRNotificationDuration.default.rawValue, onTap: nil, onTimeout: nil))
    }
    
}
