//
//  User.swift
//  train-your-mind
//
//  Created by sasha ataman on 30.04.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation

struct User {
    
    var email: String
    var password: String
    var name: String
    
    init(email: String = "", password: String = "", name: String = "") {
        self.email = email
        self.password = password
        self.name = name
    }
    
}
