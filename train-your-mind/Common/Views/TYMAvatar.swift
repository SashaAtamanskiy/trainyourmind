//
//  TYMAvatar.swift
//  train-your-mind
//
//  Created by Alex on 12.05.2017.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import UIKit

enum RoundingMode {
    case circle
    case withRadius(CGFloat)
}

class TYMAvatar: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        applyRounding(mode: .circle)
    }

}

extension UIView {

    func applyRounding(mode: RoundingMode) {
        let radius = radiusWith(mode: mode)
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    private func radiusWith(mode: RoundingMode) -> CGFloat {
        switch mode {
        case .circle:
            return self.bounds.size.width / 2.0
        case let .withRadius(value):
            return value
        }
    }
}
