//
//  UINavigationBarSetuable.swift
//  train-your-mind
//
//  Created by sasha ataman on 01.05.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//
import UIKit

protocol NavigationBarSetupable: class {
    func dissmissViewController()
    func popViewController()
}

extension NavigationBarSetupable where Self: UIViewController {
    
    @discardableResult
    func setupNavigationBar(
        title: String,
        showDismiss: Bool = false,
        showTopLogo: Bool = false)
        -> (UINavigationBar, UINavigationItem)
    {
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 64))
        let navItem = UINavigationItem(title: title)

        navBar.barTintColor = UIColor().hexStringToUIColor(hex: "101010")
        navBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,
                                      NSFontAttributeName: UIFont(name: "SFUIText-Regular", size: 17)!]
        
        setupLeftButton(on: navItem, showDismiss: showDismiss)
        setupTitle(on: navItem, showTopLogo: showTopLogo)
        
        navBar.setItems([navItem], animated: false)
        view.addSubview(navBar)
        navBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        navBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        navBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        view.setNeedsLayout()
        
        return (navBar, navItem)
    }
    
    private func setupTitle(on item: UINavigationItem, showTopLogo: Bool) {
        if showTopLogo {
            item.titleView = UIImageView(image: #imageLiteral(resourceName: "logo_title"))
        }
    }
    
    private func setupLeftButton(on item: UINavigationItem, showDismiss: Bool) {
        if showDismiss {
            item.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(dissmissVC))
            return
        }
        if (navigationController?.viewControllers.count ?? 0) > 1 {
            item.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(popVC))
        }
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func dissmissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

private extension UIViewController {
    
    @objc func dissmissVC() {
        guard let navSelf = self as? NavigationBarSetupable else {
            return
        }
        navSelf.dissmissViewController()
    }
    
    @objc func popVC() {
        guard let navSelf = self as? NavigationBarSetupable else {
            return
        }
        navSelf.popViewController()
    }
    
}
