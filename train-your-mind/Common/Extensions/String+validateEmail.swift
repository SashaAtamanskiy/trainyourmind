//
//  String+validateEmail.swift
//  train-your-mind
//
//  Created by sasha ataman on 10.05.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func validateEmail() -> Bool {
        let expression = "^[-\\w.]+@([A-z0-9]+\\.)+[A-z]{2,4}$"
        
        let emailTest: NSPredicate  = NSPredicate(format: "SELF MATCHES[c] %@", expression)
        
        let finishResult = emailTest.evaluate(with: self)
        return finishResult
    }
    
}
