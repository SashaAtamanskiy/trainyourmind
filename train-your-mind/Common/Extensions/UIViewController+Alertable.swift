//
//  UIViewController+Alertable.swift
//  train-your-mind
//
//  Created by sasha ataman on 30.04.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import UIKit

protocol Alertable: NSObjectProtocol {
    func showAlertWithTitle(_ title: String?, message: String?, actions: [UIAlertAction])
    func showAlertWithTitle(_ title: String?, message: String?)
}

extension Alertable where Self: UIViewController {
    
    func showAlertWithTitle(_ title: String?, message: String?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        actions.forEach{ (action) in
            alertController.addAction(action)
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithTitle(_ title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) {_ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
