//
//  KeychainService.swift
//  train-your-mind
//
//  Created by sasha ataman on 12.05.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import KeychainAccess

class KeychainService {
    
    func userId() -> Bool {
        let keychain = Keychain(service: "com.train-your-mind.com").accessibility(.alwaysThisDeviceOnly)
            .synchronizable(false)
        
        do {
            let id = try keychain.getString("user_id")
            if id != nil {
                return true
            }
        } catch {
            print("no id")
        }
        
        return false
    }
    
    func save(id: String) {
        let userId = Keychain(service: "com.train-your-mind.com")
        userId["user_id"] = id
    }
    
    func clearId() {
        let keychain = Keychain(service: "com.train-your-mind.com")
        keychain["user_id"] = nil
    }
    
    func getId() -> String {
        let keychain = Keychain(service: "com.train-your-mind.com")
        guard let id = keychain["user_id"] else {
            return ""
        }
        return id
    }
    
}
