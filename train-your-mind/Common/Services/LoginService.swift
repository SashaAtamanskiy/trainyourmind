//
//  LoginService.swift
//  train-your-mind
//
//  Created by sasha ataman on 30.04.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import Alamofire

class LoginService: LoginServiceProtocol {
    
    fileprivate let manager: ApiManagerProtocol
    
    init(manager: ApiManagerProtocol = SessionManager.default) {
        self.manager = manager
    }
    
    func fetch(user: User, completion: @escaping (Bool, String?) -> ()) {
        let _ = manager.apiRequest(.login, parameters: ["email": user.email,
                                                        "password": user.password],
                                                        headers: nil)
            .apiResponse(completionHandler: {
                response in
                switch response.result {
                case .success(let json):
                    print(json)
                    if !json["error"].boolValue {
                        completion(!json["error"].boolValue, json["body"]["id"].stringValue)
                        return
                    }
                    completion(false, nil)
                    break
                case .failure(let error):
                    print(error)
                    completion(false, nil)
                    break
                }
            })
    }
    
    func add(user: User, completion: @escaping (Bool, String?) -> ()) {
        let _ = manager.apiRequest(.signUp, parameters: ["email": user.email,
                                                         "password": user.password,
                                                         "name": user.name],
                                                        headers: nil)
            .apiResponse(completionHandler: {
                response in
                switch response.result {
                case .success(let json):
                    print(json)
                    if !json["error"].boolValue {
                        completion(!json["error"].boolValue, json["body"]["id"].stringValue)
                        return 
                    }
                    completion(false, nil)
                    break
                case .failure(let error):
                    print(error)
                    completion(false, nil)
                    break
                }
            })
    }
    
}
