//
//  ProfileServiceProtocol.swift
//  train-your-mind
//
//  Created by sasha ataman on 12.05.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation

protocol ProfileServiceProtocol {
    func getUser(from id: String, completion: @escaping (Bool, User?) -> ())
    func update(_ user: User, id: String, completion: @escaping (Bool, User?) -> ())
}
