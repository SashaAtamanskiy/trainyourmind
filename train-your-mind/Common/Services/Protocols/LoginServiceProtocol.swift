//
//  LoginServiceProtocol.swift
//  train-your-mind
//
//  Created by sasha ataman on 30.04.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation

protocol LoginServiceProtocol {
    func fetch(user: User, completion: @escaping (Bool, String?) -> ())
    func add(user: User, completion: @escaping (Bool, String?) -> ())
}
