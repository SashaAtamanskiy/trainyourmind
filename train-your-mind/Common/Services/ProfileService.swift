//
//  ProfileService.swift
//  train-your-mind
//
//  Created by sasha ataman on 12.05.17.
//  Copyright © 2017 sasha ataman. All rights reserved.
//

import Foundation
import Alamofire

class ProfileService: ProfileServiceProtocol {
    
    fileprivate let manager: ApiManagerProtocol
    
    init(manager: ApiManagerProtocol = SessionManager.default) {
        self.manager = manager
    }
    
    func getUser(from id: String, completion: @escaping (Bool, User?) -> ()) {
        let _ = manager.apiRequest(.getCurrentUser, parameters: ["id": id], headers: nil)
            .apiResponse(completionHandler: {
                response in
                switch response.result {
                case .success(let json):
                    if !json["error"].boolValue {
                        var user = User()
                        let body = json["body"]
                        user.email = body["email"].stringValue
                        user.password = body["password"].stringValue
                        user.name = body["name"].stringValue
                        completion(true, user)
                    }
                    completion(false, nil)
                    break
                case .failure(let error):
                    completion(false, nil)
                    print(error)
                    break
                }
            })
    }
    
    func update(_ user: User, id: String, completion: @escaping (Bool, User?) -> ()) {
        let _ = manager.apiRequest(.updateUser, parameters: ["email": user.email,
                                                             "name": user.name,
                                                             "password": user.password,
                                                             "id": id],
                                   headers: nil)
            .apiResponse(completionHandler: {
                response in
                switch response.result {
                case .success(let json):
                    if !json["error"].boolValue {
                        var user = User()
                        let body = json["body"]
                        user.email = body["email"].stringValue
                        user.password = body["password"].stringValue
                        user.name = body["name"].stringValue
                        completion(true, user)
                    }
                    completion(false, nil)
                    break
                case .failure(let error):
                    completion(false, nil)
                    print(error)
                    break
                }
            })
    }
    
}
