//
//  MainViewController.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 09/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile
import KeychainAccess

// MARK: - MainViewController

final class MainViewController: UIViewController, FlowController, NavigationBarSetupable {

    // MARK: - VIPER stack

    var presenter: MainViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: "Train")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}

// MARK: - MainViewInput

extension MainViewController: MainViewInput {
    
}
