//
//  MainRouter.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 09/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile
import Perform

// MARK: - MainRouter

final class MainRouter {

    // MARK: - VIPER stack

    weak var flowController: FlowController!

}

// MARK: - MainRouterInput

extension MainRouter: MainRouterInput {
    
}

