//
//  MainPresenterInterface.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 09/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile

// MARK: - MainViewOutput

protocol MainViewOutput: class {

}

// MARK: - MainInteractorOutput

protocol MainInteractorOutput: class {

}

// MARK: - MainModuleInput

protocol MainModuleInput: ModuleInput {

}

// MARK: - MainModuleOutput

protocol MainModuleOutput: ModuleOutput {

}
