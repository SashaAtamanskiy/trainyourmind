//
//  MainPresenter.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 09/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile

// MARK: - MainPresenter

final class MainPresenter {

    // MARK: - VIPER stack

    weak var moduleOutput: ModuleOutput?
    weak var view: MainViewInput!
    var interactor: MainInteractorInput!
    var router: MainRouterInput!
    
}

// MARK: - MainViewOutput

extension MainPresenter: MainViewOutput {

}

// MARK: - MainInteractorOutput

extension MainPresenter: MainInteractorOutput {
    
}

// MARK: - MainModuleInput

extension MainPresenter: MainModuleInput {

}
