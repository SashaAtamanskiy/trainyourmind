//
//  MainInteractor.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 09/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - MainInteractor

import NVActivityIndicatorView

final class MainInteractor {

    // MARK: - VIPER stack

    weak var presenter: MainInteractorOutput!

    // MARK: -

}

// MARK: - MainInteractorInput

extension MainInteractor: MainInteractorInput {

}
