//
//  ProfilePresenter.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile
import KeychainAccess

// MARK: - ProfilePresenter

final class ProfilePresenter {

    // MARK: - VIPER stack

    weak var moduleOutput: ModuleOutput?
    weak var view: ProfileViewInput!
    var interactor: ProfileInteractorInput!
    var router: ProfileRouterInput!
    
    // MARK: - Property
    
    var user: User?

}

// MARK: - ProfileViewOutput

extension ProfilePresenter: ProfileViewOutput {
    
    func fetchUser(from id: String) {
        interactor.fetchUser(from: id)
    }
    
    func getUserId() -> String {
        let service = KeychainService()
        return service.getId()
    }
    
    func update(user: User) {
        interactor.update(user, id: self.getUserId())
    }

}

// MARK: - ProfileInteractorOutput

extension ProfilePresenter: ProfileInteractorOutput {
    
    func set(user: User) {
        self.user = user
        view.reloadData(with: user)
    }

}

// MARK: - ProfileModuleInput

extension ProfilePresenter: ProfileModuleInput {

}
