//
//  ProfileRouter.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile

// MARK: - ProfileRouter

final class ProfileRouter {

    // MARK: - VIPER stack

    weak var flowController: FlowController!

}

// MARK: - ProfileRouterInput

extension ProfileRouter: ProfileRouterInput {

}
