//
//  ProfileViewController.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile
import SkyFloatingLabelTextField

// MARK: - ProfileViewController

final class ProfileViewController: UIViewController, FlowController, NavigationBarSetupable {

    // MARK: - VIPER stack
    var presenter: ProfileViewOutput!

    // MARK:- Outlets

    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var name: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var profile: UIImageView!
    
    // MARK:- UIBarButtonItems
    
    fileprivate let edit = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .done, target: self, action: #selector(editProfile(sender:)))
    fileprivate let logout = UIBarButtonItem(image: #imageLiteral(resourceName: "logout"), style: .done, target: self, action: #selector(logOut))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.fetchUser(from: presenter.getUserId())

        addBarButtonItems()
        setProfileBorder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.disableAllField()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    func addBarButtonItems() {
        let ( _, barItems) = setupNavigationBar(title: "Profile")

        edit.tintColor = .white
        barItems.rightBarButtonItem = edit
        logout.tintColor = .white
        barItems.leftBarButtonItem = logout
    }

    func setProfileBorder() {
        profile.layer.borderColor = UIColor.white.cgColor
        profile.layer.borderWidth = 1.5
    }

    func editProfile(sender: UIBarButtonItem) {
        sender.image = sender.image == #imageLiteral(resourceName: "edit") ? #imageLiteral(resourceName: "cancel") : #imageLiteral(resourceName: "edit")
        [name, password, email].forEach{
            if let butt = $0 { butt.isUserInteractionEnabled = !(butt.isUserInteractionEnabled);
                butt.isHighlighted = butt.isUserInteractionEnabled } }
        save.alpha = name.isHighlighted ? 1.0 : 0.5
        save.isEnabled = name.isHighlighted
    }

    func logOut() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func update(_ sender: Any) {
        var user = User()
        user.email = email.text!
        user.name = name.text!
        user.password = password.text!
        presenter.update(user: user)
    }
    
    func disableAllField() {
        [name, password, email].forEach{
            if let butt = $0 { butt.isUserInteractionEnabled = false
            butt.isHighlighted = false } }
        save.alpha = 0.5
        save.isEnabled = false
        edit.image = #imageLiteral(resourceName: "edit")
    }
    

}

// MARK: - ProfileViewInput

extension ProfileViewController: ProfileViewInput {
    
    func reloadData(with user: User) {
        email.text = user.email
        password.text = user.password
        name.text = user.name
        self.disableAllField()
    }

}
