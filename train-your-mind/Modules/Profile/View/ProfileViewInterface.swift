//
//  ProfileViewInterface.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - ProfileViewInput

protocol ProfileViewInput: class {
    func reloadData(with user: User)
}
