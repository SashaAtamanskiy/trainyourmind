//
//  ProfileAssember.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Foundation

// MARK: - ProfileAssembler

final class ProfileAssembler: NSObject {

    @IBOutlet weak var view: ProfileViewController!

    override func awakeFromNib() {
        super.awakeFromNib()

        let interactor = ProfileInteractor()
        let presenter = ProfilePresenter()
        let router = ProfileRouter()

        view.presenter = presenter
        view.moduleInput = presenter
        interactor.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        router.flowController = view
    }

}
