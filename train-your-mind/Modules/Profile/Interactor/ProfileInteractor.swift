//
//  ProfileInteractor.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - ProfileInteractor

import NVActivityIndicatorView

final class ProfileInteractor {

    // MARK: - VIPER stack

    weak var presenter: ProfileInteractorOutput!

    // MARK: -
    
    fileprivate let profileManager: ProfileServiceProtocol
    fileprivate let activityData = ActivityData()
    
    init(profileManager: ProfileServiceProtocol) {
        self.profileManager = profileManager
    }
    
    convenience init() {
        self.init(profileManager: ProfileService())
    }

}

// MARK: - ProfileInteractorInput

extension ProfileInteractor: ProfileInteractorInput {
    
    func fetchUser(from id: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        profileManager.getUser(from: id, completion: {
            [weak self] error, user in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if let user = user {
                error ? self?.presenter.set(user: user) :
                    Notification.shared.showErrorAler(with: "Error".uppercased(),
                                                      body: "User not found".uppercased())

            }
        })
    }
    
    func update(_ user: User, id: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        profileManager.update(user, id: id, completion: {
            [weak self] error, user in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if let user = user {
                error ? Notification.shared.showSuccessAlert(with: "Success".uppercased(), body: "User Updated".uppercased()) : ()
                error ? self?.presenter.set(user: user) :
                    Notification.shared.showErrorAler(with: "Error".uppercased(),
                                                      body: "User not found".uppercased())
                
            }
        })
    }

}
