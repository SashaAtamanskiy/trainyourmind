//
//  ProfileInteractorInterface.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 11/05/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - ProfileInteractorInput

protocol ProfileInteractorInput: class {
    func fetchUser(from id: String)
    func update(_ user: User, id: String)
}
