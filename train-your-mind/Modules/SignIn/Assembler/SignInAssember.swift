//
//  SignInAssember.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Foundation

// MARK: - SignInAssembler

final class SignInAssembler: NSObject {

    @IBOutlet weak var view: SignInViewController!

    override func awakeFromNib() {
        super.awakeFromNib()

        let interactor = SignInInteractor()
        let presenter = SignInPresenter()
        let router = SignInRouter()

        view.presenter = presenter
        view.moduleInput = presenter
        interactor.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        router.flowController = view
    }
}
