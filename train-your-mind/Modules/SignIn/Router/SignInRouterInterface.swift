//
//  SignInRouterInterface.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - SignInRouterInput

protocol SignInRouterInput: class {
    func openMain()
}
