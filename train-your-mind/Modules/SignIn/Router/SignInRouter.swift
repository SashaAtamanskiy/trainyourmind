//
//  SignInRouter.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile
import Perform

// MARK: - SignInRouter

final class SignInRouter {
    
    // MARK: - VIPER stack
    
    weak var flowController: FlowController!
    
}

// MARK: - SignInRouterInput

extension SignInRouter: SignInRouterInput {
    
    func openMain() {
        flowController.openModule(using: .openMain) {
            guard let _ = $0 as? MainModuleInput else { fatalError() }
            return nil
        }
    }
    
}

extension Segue {
    
    static var openMain: Segue<MainViewController> {
        return .init(identifier: "transitionToMain")
    }
    
}
