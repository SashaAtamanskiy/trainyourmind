//
//  SignInViewController.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile
import SJFluidSegmentedControl
import LNRSimpleNotifications

// MARK: - SignInViewController

final class SignInViewController: UIViewController, FlowController, Alertable, NavigationBarSetupable {
    
    // MARK: - VIPER stack
    
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var fluentSegmentedControl: SJFluidSegmentedControl!
    
    // MARK: - Constraints
    
    @IBOutlet weak var loginBottom: NSLayoutConstraint!
    @IBOutlet weak var nameTop: NSLayoutConstraint!
    
    var presenter: SignInViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(title: "TrainYourMind")
        fluentSegmentedControl.textFont = UIFont(name: "SFUIText-Regular", size: 15)!
    }

    @IBAction func login(_ sender: Any) {
        var user = User()
        user.email = email.text!
        user.password = password.text!
        user.name = name.text!
        fluentSegmentedControl.currentSegment == 0 ? presenter.fetch(user: user) :
                                                     presenter.add(user: user)
    }
    
    func activeNameConstraint() {
        UIView.animate(withDuration: 0.5, delay: 0.1, options: [.curveEaseInOut], animations: {
            self.loginBottom.isActive = false
            self.nameTop.isActive = true
            self.view.layoutIfNeeded()
            self.login.setTitle("SignUp", for: .normal)
            self.name.isHidden = false
        }, completion: nil)
    }
    
    func deactivateNameConstraint() {
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: [.curveEaseInOut], animations: {
            self.loginBottom.isActive = true
            self.nameTop.isActive = false
            self.view.layoutIfNeeded()
            self.login.setTitle("SignIn", for: .normal)
        }, completion: { _ in
            self.name.isHidden = true
        })
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

// MARK: - SignInViewInput

extension SignInViewController: SignInViewInput {
    
    func showAlert(with message: String) {
        showAlertWithTitle("", message: message)
    }
    
}

// MARK:- UITextViewDelegate

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

// MARK:- SJFluidSegmentedControlDataSource

extension SignInViewController: SJFluidSegmentedControlDataSource {
    
    func numberOfSegmentsInSegmentedControl(_ segmentedControl: SJFluidSegmentedControl) -> Int {
        return 2
    }
    
}

// MARK:- SJFluidSegmentedControlDelegate

extension SignInViewController: SJFluidSegmentedControlDelegate {
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl, didChangeFromSegmentAtIndex fromIndex: Int, toSegmentAtIndex toIndex: Int) {
        toIndex == 1 ? activeNameConstraint() : deactivateNameConstraint()
    }
    
    func segmentedControl(_ segmentedControl: SJFluidSegmentedControl, titleForSegmentAtIndex index: Int) -> String? {
        return index == 0 ? "Login" : "Registration"
    }
    
}
