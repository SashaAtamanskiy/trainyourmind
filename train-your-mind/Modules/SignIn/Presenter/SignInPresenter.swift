//
//  SignInPresenter.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile

// MARK: - SignInPresenter

final class SignInPresenter {

    // MARK: - VIPER stack

    weak var moduleOutput: ModuleOutput?
    weak var view: SignInViewInput!
    var interactor: SignInInteractorInput!
    var router: SignInRouterInput!

}

// MARK: - SignInViewOutput

extension SignInPresenter: SignInViewOutput {
    
    func fetch(user: User) {
        validate(with: user.email) ? interactor.fetch(user: user)
                                     : Notification.shared.showErrorAler(with: "Error".uppercased(), body: "Invalid email".uppercased())
    }
    
    func add(user: User) {
        if user.name.characters.count > 0 {
            validate(with: user.email) ? interactor.add(new: user)
                                     : Notification.shared.showErrorAler(with: "Error".uppercased(), body: "Invalid email".uppercased())
        } else {
            Notification.shared.showErrorAler(with: "Error".uppercased(), body: "Name is required".uppercased())
        }
    }
    
    func validate(with text: String) -> Bool {
        return text.validateEmail()
    }
    
}

// MARK: - SignInInteractorOutput

extension SignInPresenter: SignInInteractorOutput {

    func showAlert(message: String) {
        Notification.shared.showErrorAler(with: "Error".uppercased(), body: message.uppercased())
    }
    
    func openMain(id: String?) {
        if let id = id {
            let service = KeychainService()
            service.save(id: id)
        }
        router.openMain()
    }
    
}

// MARK: - SignInModuleInput

extension SignInPresenter: SignInModuleInput {

}
