//
//  SignInPresenterInterface.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

import Chamomile

// MARK: - SignInViewOutput

protocol SignInViewOutput: class {
    func fetch(user: User)
    func add(user: User)
}

// MARK: - SignInInteractorOutput

protocol SignInInteractorOutput: class {
    func showAlert(message: String)
    func openMain(id: String?)
}

// MARK: - SignInModuleInput

protocol SignInModuleInput: ModuleInput {

}

// MARK: - SignInModuleOutput

protocol SignInModuleOutput: ModuleOutput {

}
