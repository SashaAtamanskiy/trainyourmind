//
//  SignInInteractor.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - SignInInteractor

import NVActivityIndicatorView

final class SignInInteractor {

    // MARK: - VIPER stack

    weak var presenter: SignInInteractorOutput!

    // MARK: -
    
    fileprivate let loginManager: LoginServiceProtocol
    fileprivate let activityData = ActivityData()
    
    init(loginManager: LoginServiceProtocol) {
        self.loginManager = loginManager
    }
    
    convenience init() {
        self.init(loginManager: LoginService())
    }

}

// MARK: - SignInInteractorInput

extension SignInInteractor: SignInInteractorInput {
    
    func fetch(user: User) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        loginManager.fetch(user: user, completion: {
            [weak self] error, id in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            self?.handle(error: error, id: id)
        })
    }
    
    func add(new user: User) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        loginManager.add(user: user, completion: {
            [weak self] error, id in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            self?.handle(error: error, id: id)
        })
    }
    
    func handle(error: Bool, id: String?) {
        error ? self.presenter.openMain(id: id) : self.presenter.showAlert(message: "Wrong email or password")
    }
    
}
