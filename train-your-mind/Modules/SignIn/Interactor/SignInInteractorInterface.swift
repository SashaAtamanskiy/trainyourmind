//
//  SignInInteractorInterface.swift
//  train-your-mind
//
//  Created by Sasha Atamanskiy on 29/04/2017.
//  Copyright © 2017 MoziDev. All rights reserved.
//

// MARK: - SignInInteractorInput

protocol SignInInteractorInput: class {
    func fetch(user: User)
    func add(new user: User)
}
